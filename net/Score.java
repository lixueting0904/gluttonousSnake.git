package team.snack.net;

import java.awt.*;
import javax.swing.*;
import javax.swing.event.*;

import team.snack.game.Game;

/**
 * 得分相关
 *
 * @version 1.0.0
 * @author Troy<troy@tencoe.com>
 * @since 4.1
 */
public class Score {
    /**
     * 上传得分至排行榜
     *
     * @param name  昵称
     * @param score 得分
     * @return void
     */
    public static void upload(String name, int score) {
    	int len = 10;
        if (Game.snake.length() > len) {
            if (!Game.invincible) {
                HttpRequest.sendPost("https://api.itroy.cc/peilun_snake/?mode=upload",
                        "name=" + name + "&score=" + score +
                        "&gamemode=normal&timestamp=" + System.currentTimeMillis());//
            }
        }

        if (Game.invincible) {
        	HttpRequest.sendPost("https://api.itroy.cc/peilun_snake/?mode=upload",
                    "name=" + name + "&score=" + score +
                    "&gamemode=normal&timestamp=" + System.currentTimeMillis());
        }
    }

    /**
     * 获取排行榜信息
     *
     * @param number    排行榜数量
     * @param gamemode  游戏模式
     * @param parameter 参数（排名，姓名，分数）
     * @return String 文本格式的排行榜
     */
    private static String get(int number, String gamemode, String parameter) {
        String response = HttpRequest.sendGet("https://api.itroy.cc/peilun_snake",
                "mode=get&number=" + number + "&gamemode=" + gamemode +
                "&parameter=" + parameter);

        return response;
    }

    /**
     * 初始化 Panel
     *
     * @param panel    Panel
     * @param gamemode 游戏模式
     * @return void
     */
    private static void initPanel(JPanel panel, String gamemode) {
    	//动态网格布局管理器
        GridBagLayout gridBagLayout = new GridBagLayout();
        panel.setLayout(gridBagLayout);

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        //使组件完全填满它的显示区域
        gridBagConstraints.fill = GridBagConstraints.BOTH;

        String numberList = get(15, gamemode, "number");

        int len = 30;
        if (numberList.length() <= len) {
            JLabel nullLabel = new JLabel("暂无排行");
            panel.removeAll();
            panel.add(nullLabel);

            return;
        }

        JLabel numberLabel = new JLabel(numberList);

        String nameList = get(15, gamemode, "name");
        JLabel nameLabel = new JLabel(nameList);

        String scoreList = get(15, gamemode, "score");
        JLabel scoreLabel = new JLabel(scoreList);

        String timeList = get(15, gamemode, "time");
        JLabel timeLabel = new JLabel(timeList);

        panel.removeAll();//从容器中删除所有组件
        panel.add(numberLabel);
        panel.add(nameLabel);
        panel.add(scoreLabel);
        panel.add(timeLabel);
        
        //横坐标
        gridBagConstraints.gridx = 0; 
        gridBagConstraints.gridy = 0;
        //宽度
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        //行的权重，通过这个属性来决定如何分配行的剩余空间
        gridBagConstraints.weightx = 1;
        gridBagLayout.setConstraints(numberLabel, gridBagConstraints);

        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.weightx = 1;
        gridBagLayout.setConstraints(nameLabel, gridBagConstraints);

        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.weightx = 1;
        gridBagLayout.setConstraints(scoreLabel, gridBagConstraints);

        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 1;
        gridBagConstraints.gridheight = 1;
        gridBagConstraints.weightx = 0;
        gridBagLayout.setConstraints(timeLabel, gridBagConstraints);
    }

    /**
     * 显示排行榜窗口
     *
     * @param parentFrame 父窗口
     * @return void
     */
    public static void show(Frame parentFrame) {
        final JDialog dialog = new JDialog(parentFrame, "排行榜", true);
        dialog.setSize(500, 400);
        dialog.setResizable(false);
        //parentFrame不为空，则放置在屏幕的中心
        dialog.setLocationRelativeTo(parentFrame);

        /** 普通模式 */
        JPanel normalPanel = new JPanel();

        /** 无敌模式 */
        JPanel invinciblePanel = new JPanel();

        //单击选项在组件之间切换
        JTabbedPane tabPanel = new JTabbedPane(JTabbedPane.TOP);

        tabPanel.addChangeListener(new ChangeListener() {
        	    @Override
                public void stateChanged(ChangeEvent evt) {
        	    	//getSelectedIndex（），返回此选项窗格的当前选定索引
                    int index = tabPanel.getSelectedIndex();

                    if (index == 0) {
                        initPanel(normalPanel, "normal");
                    }

                    if (index == 1) {
                        initPanel(invinciblePanel, "invincible");
                    }
                }
            });

        tabPanel.add(normalPanel, "普通模式");
        tabPanel.add(invinciblePanel, "无敌模式");

        //给dialog加个tabPanel
        dialog.setContentPane(tabPanel);
        dialog.setVisible(true);
    }
}
