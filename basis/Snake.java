package team.snack.basis;

import java.awt.Color;
import java.util.ArrayList;

import team.snack.game.Game;
import team.snack.net.Score;


/**
 * 蛇
 *
 * @ClassName Snake
 * @version 1.0
 * @Description [功能描述]
 * @author Java小队
 * @time 2018年1月15日
 */
public class Snake {
    ArrayList<Body> bodys = new ArrayList<Body>();

    public int length() {
        return bodys.size();
    }

    public Body getHead() {
        return bodys.get(0);
    }

    public Body getTail() {
        return bodys.get(length() - 1);
    }

    public Body get(int index) {
        return bodys.get(index);
    }

    /**
     * @Description 添加头部
     * @param body
     */
    public void addH(Body body) {
        bodys.add(0, body);
    }

    public void add(Color color) {
        int len = 0;

        if (Game.invincible) {
        	//???????????
            len = 15;
        } else {
            len = 20;
        }

//        if (length() != 0) {
//            /* 颜色A-(颜色A-颜色B)*(1-颜色A的百分比),RGB */	
//            int r = (int) (getTail().color.getRed() -
//                ((getTail().color.getRed() - color.getRed()) * 0.8));
//            int g = (int) (getTail().color.getRed() -
//                ((getTail().color.getRed() - color.getRed()) * 0.5));
//            int b = (int) (getTail().color.getRed() -
//                ((getTail().color.getRed() - color.getRed()) * 0.5));
//
//            for (int i = 0; i < 5; i++) {
//                bodys.add(new Body(getTail().x, getTail().y, new Color(r, g, b)));
//
//                if (Game.draw != null) {
//                    Game.draw.repaint();
//                }
//             } 
//
//            len -= 5;
//        }

        for (int i = 0; i < len; i++) {
            /* 新建身体 */
            if (length() != 0) {
                bodys.add(new Body(getTail().x, getTail().y, color));
            } else {
                bodys.add(new Body(0, 0, color));
               }

            if (Game.draw != null) {
                Game.draw.repaint();
            }
        }
    }

    public void remove() {
        bodys.remove(length() - 1);
    }

    /**
     *  颜色依次上移
     */
    public void moveColor() {
        for (int i = 0, len = length() - 1; i < len; i++) {
            bodys.get(i).color = bodys.get(i + 1).color;
        }
    }

    /**
     * @Description 初始化游戏
     * @param body
     */
    public void newGame() {
        System.out.println("Player: " + Game.getProperties("Name"));
        Score.upload(Game.getProperties("Name"), getScore());
        Game.speed = 12;
        Game.dir = 'R';
        Game.win = false;
        Game.speedUp = false;
        Game.food.newFood();
        bodys.clear();
        add(Color.BLACK);
        getHead().x = 0;
        getHead().y = 0;
        Game.bg.loop();
    }

    public int getScore() {
        return bodys.size() / 15;
    }

    public void del() {
        bodys.remove(length() - 1);
    }

    public boolean empty() {
        return bodys.isEmpty();
    }
}
