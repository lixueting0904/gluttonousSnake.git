package team.snack.basis;

import java.awt.Color;

/**
 * 蛇的身体
 *
 * @ClassName Body
 * @version 1.0
 * @Description [功能描述]
 * @author Java小队
 * @time 2018年1月15日
 */

public class Body {
	/**坐标
	 * 
	 */
    public int x; 
    public int y;
    /**颜色
     * 
     */
    public Color color; 

    public Body(int x, int y, Color color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    /**
     * 得到随机颜色
     *
     * @return 随机颜色
     */
    public static Color randomColor() {
        int r;
        int g;
        int b;
        double o = Math.random();
        r = (int) (o * 255);
        g = (int) (o * 255);
        b = (int) (o * 255);

        return new Color(r, g, b);
    }
}
