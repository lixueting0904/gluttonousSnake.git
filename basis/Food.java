package team.snack.basis;

import java.awt.Color;

import team.snack.game.Game;

/**
 * 食物类
 *
 * @ClassName Food
 * @version 1.0
 * @Description [功能描述]
 * @author Java小队
 * @time 2019年1月15日 
 */
public class Food {
	/**食物坐标
	 * 
	 */
    public int x; 
    /**食物坐标
     * 
     */
    public int y;  
    /**实物颜色
     * 
     */
    public Color color; 

    public void newFood() {
        // 新建食物:
        // 1.在可视范围内更新食物坐标
        x = Math.abs((int) (Math.random() * (Game.frame.getWidth() - 150))) +
            20;
        y = Math.abs((int) (Math.random() * (Game.frame.getHeight() - 150))) +
            20;
        // 2.随机生成食物颜色
        color = Body.randomColor();
    }
}
