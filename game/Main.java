package team.snack.game;

/**
 * @ClassName Main
 * @version 1.0
 * @author Java小队
 * @time 2018年1月15日
 */
public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        game.go(); // 开始游戏
    }
}
