package team.snack.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

import team.snack.basis.Body;

/**
 * @Description 游戏面板
 * @ClassName Draw
 * @author Java小队
 * @version 4.2
 */
@SuppressWarnings("serial")
public class Draw extends JPanel {
	double a = 1.5;
	/**食物闪动是否为红色
	 * 
	 */
	boolean red = false; 
	/**判断食物闪动
	 * 
	 */
    int num = 0; 
    
    @Override
    protected void paintComponent(Graphics graphics) {
    	// 把 Graphics 转为 Graphics2D
        Graphics2D g = (Graphics2D) graphics; 
        // 消除文字锯齿	
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
            RenderingHints.VALUE_TEXT_ANTIALIAS_ON); 
        // 消除画图锯齿
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON); 
        g.setColor(Game.backColor);
        g.fillRect(0, 0, getWidth(), getHeight());

        Body body; // 身体循环变量

        for (int i = (Game.snake.length() - 1); i >= 0; i--) {
            // 从后至前绘制身体
            try {
                body = Game.snake.get(i);
                //新的渲染颜色
                g.setColor(body.color);
                //填充椭圆
                g.fillOval(body.x, body.y, 28, 28);
            } catch (Exception e) {
            }
        }

        // 绘制食物
        g.setColor(Game.food.color);
        g.fillRect(Game.food.x, Game.food.y, 25, 25);
        g.setColor(new Color(255, 0, 0));

        if (red) {
            // 此处为食物闪动模块，红绿交替
        	//红
            g.setColor(new Color(255, 0, 0));

            if (num < (Game.speed * a)) {
                num++;
            } else {
                red = false;
                num = 0;
            }
        } else {
        	//绿
            g.setColor(new Color(0, 255, 0));
//            g.setColor(new Color(0, 0, 255));//蓝                

            if (num < (Game.speed * a)) {
                num++;
            } else {
                red = true;
                num = 0;
            }
        }

        // 画食物边界
        g.drawRect(Game.food.x, Game.food.y, 25, 25);
        // 绘制分数提示
        g.setFont(new Font("楷体", Font.PLAIN, 25));

        if (Game.win) {
            // 分数大于50分时颜色为橘黄色,否则为黑色
            g.setColor(new Color(213, 154, 0));
        } else {
            g.setColor(new Color(0, 0, 0));
        }

        //str,x,y
        g.drawString("分数:" + Game.snake.getScore(), 0, 25);

        if (Game.speedUp) {
            g.setColor(Color.BLUE);
            g.drawString("加速中...", 0, 50);
        }
    }
}

