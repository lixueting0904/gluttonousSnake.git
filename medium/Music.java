package team.snack.medium;

import java.applet.*;
import java.io.File;
import java.net.*;

import team.snack.game.Game;

/**
 * 播放背景音乐
 *
 * @ClassName PlayMusic
 * @version 1.0
 * @author Java小组
 * @time 2019年1月15日 
 */
public class Music {
    private AudioClip sound;

    public Music(String musicName) {
        File soundFile = new File("src/team/snack/medium/" + musicName + ".wav");

        URL url = null;

        try {
            URI uri = soundFile.toURI();
            url = uri.toURL();
        } catch (MalformedURLException ex) {
        }

        sound = Applet.newAudioClip(url);
    }

    public void play() {
        if (Game.music) {
            sound.play();
        }
    }

    public void stop() {
        if (Game.music) {
            sound.stop();
        }
    }

    public void loop() {
        if (Game.music) {
            sound.loop();
        }
    }
}
